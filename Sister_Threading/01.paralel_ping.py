# import os, re dan threading
import os
import re
import threading
import subprocess
# import time
import time

# buat kelas ip_check
class ip_check(threading.Thread):
    
    # fungsi __init__; init untuk assign IP dan hasil respons = -1
    def __init__ (self,ip):
        threading.Thread.__init__(self)
        self.ip = ip
        self.hasil = -1
    
    # fungsi utama yang diekseskusi ketika thread berjalan
    def run(self):
        # lakukan ping dengan perintah ping -n (gunakan os.popen())
        ping_out = os.popen("Ping -n 2 " + self.ip, "r")
        # loop forever
        while True:
            # baca hasil respon setiap baris
            line = ping_out.readline()
            # break jika tidak ada line lagi
            if not line:
                break
            
            # baca hasil per line dan temukan pola Received = x
            recv = re.findall(recv_packages,line)
            # tampilkan hasilnya
            if recv:
                self.hasil = int(recv[0])
                print((self.ip + ": " + self.status()))
                
    # fungsi untuk mengetahui status; 0 = tidak ada respon, 1 = hidup tapi ada loss, 2 = hidup
    def status(self):
        # 0 = tidak ada respon
        if self.hasil == 1:
            return "tidak ada respon"
        
        # 1 = ada loss
        elif self.hasil == 1:
            return "ada loss"
        # 2 = hidup
        elif self.hasil == 2:
            return "hidup"
        # -1 = seharusnya tidak terjadi
        else:
            return "seharusnya tidak terjadi"    
# buat regex untuk mengetahui isi dari r"Received = (\d)"
recv_packages = re.compile(r"Received = (\d)")
# catat waktu awal
start = time.time()

# buat list untuk menampung hasil pengecekan
res = []
# lakukan ping untuk 20 host
for suffix in range(1,21):
    # tentukan IP host apa saja yang akan di ping
    ips = "192.168.8."+str(suffix)
    
    # panggil thread untuk setiap IP
    current = ip_check(ips)    
    
    # masukkan setiap IP dalam list
    res.append(current)
    
    # jalankan thread
    current.start()

# untuk setiap IP yang ada di list
for el in res:
    
    # tunggu hingga thread selesai
    el.join()
    
    # dapatkan hasilnya
    print("status from ", el.ip, "is",el.status())

# catat waktu berakhir
end = time.time()

# tampilkan selisih waktu akhir dan awal
print(end-start)