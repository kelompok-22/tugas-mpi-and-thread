# import mpi4py
from mpi4py import MPI

# buat COMM
comm = MPI.COMM_WORLD

# dapatkan rank proses
rank = comm.Get_rank()
# dapatkan total proses berjalan
size = comm.Get_size()

if rank == size-1:
    data = "Pesan dari proses " + str(rank)
    for i in range(0, size-1):
        comm.send(data, dest=i, tag=i)
        print('Process {} sent data:'.format(rank), data)
else:
    data = comm.recv(source=size-1, tag=rank)
    print('Process {} received data:'.format(rank), data)

