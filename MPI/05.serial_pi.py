#import time untuk menghitung waktu 
import time

#membaut fungsi pi dengan parameter jumlah langkah
def Pi(num_steps):
    start = time.time() # start waktu
    step = 1.0/num_steps #hitung step
    sum = 0 #var untuk menampung nilai dari sum
    for i in range(num_steps): #loop sebanyak langkah
        x= (i+0.5)*step #menentukan x 
        sum = sum + 4.0/(1.0+x*x) #menambat nilai sum
    pi = step * sum #menghitung pi
    end = time.time() #akhiri waktu program
    print ("Pi with %d steps is %f in %f secs" %(num_steps, pi, end-start)) #tampilkan hasil

#fungsi main
if __name__ == '__main__':
    Pi(100000) #jalankan fungsi pi dengan 10000 langkah
